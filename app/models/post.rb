class Post < ApplicationRecord
  has_many :comments, dependent: :destroy, as: :commentable

  validates :text, presence: true
  validates :title, length: { minimum: 2, maximum: 255 }
end
