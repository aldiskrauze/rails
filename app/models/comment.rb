class Comment < ApplicationRecord
  belongs_to :commentable, polymorphic: true
  has_many :comments, dependent: :destroy, as: :commentable

  validates :user, :body, presence: true

  def post
    return @post if defined?(@post)
    @post = commentable.is_a?(Post) ? commentable : commentable.post
  end
end
