class CommentsController < ApplicationController
  before_action :find_commentable

  def new
    @comment = Comment.new
  end

  def create
    @comment = @commentable.comments.build(comment_params)

    if @comment.save
      respond_to do |format|
        format.html { redirect_to post_path(@comment.post), :notice => 'Comment saved!' }
        format.js
      end

    else
      render 'new'
    end

  end

  def edit
    @comment = @commentable.comments.find(params[:id])
  end

  def update
    @comment = @commentable.comments.find(params[:id])
    if @comment.update_attributes(comment_params)
      flash[:notice] = "Comment updated!"
      redirect_to post_path(@commentable)
    else
      render 'edit'
    end
  end

  def destroy
    @comment = @commentable.comments.find(params[:id])
    @comment.destroy
    flash[:alert] = "Comment deleted!"
    redirect_to post_path(@comment.post)
  end

  private

  def comment_params
    params.require(:comment).permit(:user, :body)
  end

  def find_commentable
    @commentable = Post.find(params[:post_id]) if params[:post_id]
    @commentable = Comment.find(params[:comment_id]) if params[:comment_id]

    redirect_to root_path unless defined?(@commentable)
  end
end
