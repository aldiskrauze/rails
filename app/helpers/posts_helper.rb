module PostsHelper
  def post_comments_count(post)
    Post.find(post.id).comments.count
  end
end
